/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {



    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
    var request = require('request');
    var bearer = "ODZiMDdkYTYwN2M5YzcyZGJiMmEzMWFiMDc5YWNkNzEwODU3ZjZlZmUzNmY4MmVhNzc4M2NkNzJkZGFmYmVlYQ" ;
    var client_id = "660133_4ubtq3dp9nacgcg4488s04ssgwsokww0kookkso4kso44c48w4" ;
    var baseURL = "https://web.mention.net/api";
    var headers = {
        'Authorization': ' Bearer '+bearer ,
        'Accept-Version': '1.8'
    };

    //get all alerts
    var Promise = require('promise');
    var promise = new Promise(function (resolve, reject) {
      request
        .get({
              url : baseURL+'/accounts/'+client_id+'/alerts' ,
              headers : headers
        },function(error,response,body){
            if(error) reject(error);
            else{
                var jsonBody = JSON.parse(body);
                var alerts = [] ;
                for (var i = 0 ; i <  jsonBody.alerts.length ; i++) {
                  alerts.push({ id : jsonBody.alerts[i]['id']  , name : jsonBody.alerts[i]['name']  });
                }
                resolve(alerts);
            }
            console.log("started-2");

        });

    });


    var fulldata = {} ;
    var obj = "";
    var theRequest = null ;
    promise.then(function(data){
      var alerts = "?" ;
      fulldata = data ;
      for(var i = 0 ; i < data.length ; i++){
          alerts = alerts + "&alerts[]="+data[i]['id'] ;
      }
      console.log("started-3");
      theRequest = request
        .get({
              url : 'https://stream.mention.net/api/accounts/'+client_id+'/mentions'+alerts ,
              headers : headers
        })
        .on('data', function(data) {
                // compressed data as it is received
                const StringDecoder = require('string_decoder').StringDecoder;
                const decoder = new StringDecoder('utf8');
                data = decoder.write(data);

                if(data.indexOf('"type":"mark"')==-1){
                    obj += data;
                    if(IsJsonString(obj)){
                        var final_obj = JSON.parse(obj);
                        obj = "";
                        if(final_obj["mention"] && final_obj["mention"]["id"]){delete final_obj["mention"].id;}
                        if(final_obj["mention"]){
                            if(final_obj["mention"]["author_influence"]){
                                final_obj["mention"]["author_influence"] = JSON.stringify(final_obj["mention"]["author_influence"]);
                            }
                            if(final_obj["mention"]["metadata"]){
                                final_obj["mention"]["metadata"] = JSON.stringify(final_obj["mention"]["metadata"]);
                            }
                            if(sails.config.sockets.ws && sails.config.sockets.wss){
                                sails.config.sockets.wss.clients.forEach(function(client) {
                                    client.send(JSON.stringify(final_obj["mention"]));
                                });
                            }
                            Mentions.create(final_obj["mention"]).exec(function (err, finn){

                            console.log(err);
                        })};
                    }else{
                        console.log("$$$$$$$");
                        console.log(obj);
                        console.log("$$$$$$$");
                    }

                  console.log(data);
                }else{

                  console.log('ignore mark');
                }

        });
    },function(data){
        //console.log(data);
        //console.log("error");
    });
  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)

  cb();
};
