/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
      alert_id: {
          type: 'string',
          unique: true
      },
      user_id: {
          type: 'string',
          unique: true
      },
      token: {
          type: 'string'
      },
  },
  tableName: 'users',
  beforeCreate: function (values, cb) {
    User.destroy({alert_id:values.alert_id,user_id:values.user_id}).exec(function (err) {
        cb();
    });
 }
};
