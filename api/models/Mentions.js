/**
 * Mentions.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
      alert_id: {
          type: 'string'
      },
      country: {
          type: 'string'
      },
      clickable_url: {
          type: 'string'
      },
      description: {
          type: 'text'
      },
      description_medium: {
          type: 'text'
      },
      description_short: {
          type: 'text'
      },
      display_site: {
          type: 'string'
      },
      displayable_url: {
          type: 'string'
      },
      domain_reach: {
          type: 'integer'
      },
      favorite: {
          type: 'boolean'
      },
      folder: {
          type: 'string'
      },
      folder_set_by_user: {
          type: 'boolean'
      },
      important: {
          type: 'boolean'
      },
      language_code: {
          type: 'string'
      },
      original_url: {
          type: 'string'
      },
      picture_url: {
          type: 'string'
      },
      published_at: {
          type: 'string'
      },
      read: {
          type: 'boolean'
      },
      relevance_score: {
          type: 'integer'
      },
      source_name: {
          type: 'string'
      },
      source_type: {
          type: 'string'
      },
      source_url: {
          type: 'string'
      },
      title: {
          type: 'string'
      },
      tone: {
          type: 'integer'
      },
      tone_score: {
          type: 'float'
      },
      unique_id: {
          type: 'text'
      },
      updated_at: {
          type: 'datetime'
      },
      author_influence: {
          type: 'text'
      },
      source_gender:{
          type: 'string'
      },
      metadata:{
          type: 'longtext'
      },
      direct_reach:{
         type: 'integer'
      },
      cumulative_reach:{
          type: 'integer'
      }

  },
  afterCreate: function (values, cb) {
      //console.log("created-mention00000");
      User.find({"alert_id":values.alert_id}).exec(function (err,users) {
          //console.log("created-mention");
          if(users && users.length>0){
              var user = users[0];
              //console.log("created-mention2");
                var request = require('request');

                // Set the headers
                var headers = {
                    'X-AN-APP-NAME':       'newsmonitors',
                    'X-AN-APP-KEY':       '86d30525c0c5deeb0e0244506b489e7c'
                }

                // Configure the request
                var options = {
                    url: 'http://45.55.253.215:8801/api/v2/push',
                    method: 'POST',
                    headers: headers,
                    json: {
                            "device": "ios",
                            "token": user.token,
                            "alert": values.title
                    }
                }
                //console.log("request send0t");
                // Start the request
                request(options, function (error, response, body) {
                    //console.log("request sendt");
                    //console.log(response,body,error);
                    if (!error && response.statusCode == 200) {
                        // Print out the response body
                        cb();
                    }
                    cb();
                })
          }
      });
 },
  tableName: 'mentions'
};
